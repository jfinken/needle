package main

import (
	"bufio"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"runtime"
	"strconv"
	"strings"
	"time"

	"github.com/chzyer/readline"
)

// Log defines a space-delimited record representing one request to S3.
type Log struct {
	Owner     string
	Bucket    string
	ReqRecv   time.Time
	RemoteIP  string
	Requester string
	ReqID     string
	Operation string
	Key       string
	ReqURI    string
	HTTPCode  string
	ErrorCode string
	BytesSent int64
	ObjSize   int
	TotalTime time.Duration //ms
	TurnTime  time.Duration
	Referrer  string
	UserAgent string
	VersionID string
}

// Index defines the simple core index type
type Index map[string][]Log

// Yes the "values" here matter, see https://golang.org/src/time/format.go
// Go is so weird sometimes..
var parseLongForm = fmt.Sprintf("02/Jan/2006:15:04:05")
var keyLongForm = fmt.Sprintf("02/Jan/2006:15:04")

func main() {
	// For queries: readline for the prompt and history, a go implementation of
	// the gnu libreadline library.
	rl, err := readline.NewEx(&readline.Config{
		Prompt:      "query> ",
		HistoryFile: getHomeDir() + "/.needle_history",
	})

	if err != nil {
		panic(err)
	}
	defer rl.Close()

	path := os.Args[1]
	// REDUCE: map
	index := indexDirectory(path)

	// QUERY:
	output, err := os.OpenFile("./needle.results", os.O_WRONLY|os.O_APPEND|os.O_CREATE, 0600)
	if err != nil {
		panic(err)
	}
	defer output.Close()

	for {
		var query string
		var ok error
		query, ok = rl.Readline()
		if ok != nil {
			break
		}
		if query == "" {
			continue
		}
		// validate the query string
		_, err := time.Parse(keyLongForm, query)
		if err != nil {
			fmt.Fprintf(os.Stderr, "%s\n", err.Error())
			continue
		}
		// write results
		results := fmt.Sprintf("query: %s results: %d\n", query, len(index[query]))
		fmt.Fprintf(os.Stdout, "%s\n", results)
		if _, err = output.WriteString(results); err != nil {
			fmt.Fprintf(os.Stderr, "%s\n", err.Error())
		}
		logs := index[query]
		for _, log := range logs {
			result := fmt.Sprintf("%v, %s, %s, %s, %d, %s, %s\n",
				log.ReqRecv, log.RemoteIP, log.Operation, log.Key, log.BytesSent, log.HTTPCode, log.ErrorCode)
			if _, err = output.WriteString(result); err != nil {
				fmt.Fprintf(os.Stderr, "%s\n", err.Error())
			}
		}
	}
}

// index each S3 server access log found in path
func indexDirectory(path string) Index {

	fmt.Fprintf(os.Stderr, "Indexing (sequential)...\n")
	defer timeT(os.Stderr, time.Now(), "Indexing")

	index := make(Index)
	files, err := ioutil.ReadDir(path)
	if err != nil {
		log.Fatal(err)
	}

	for _, f := range files {
		file, err := os.Open(filepath.Join(path, f.Name()))
		if err != nil {
			log.Fatal(err)
		}

		scanner := bufio.NewScanner(file)
		for scanner.Scan() {
			words := strings.Fields(scanner.Text())

			if len(words) >= 16 {
				// request timestamp
				t, err := time.Parse(parseLongForm, strings.Trim(words[2], "["))
				if err != nil {
					fmt.Fprintf(os.Stderr, "%s\n", err.Error())
				}
				// swallow the error
				bs, _ := strconv.ParseInt(words[14], 10, 64)
				// total request duration. Parse requires units (ms)
				dur, _ := time.ParseDuration(fmt.Sprintf("%sms", words[16]))
				log := Log{ReqRecv: t,
					RemoteIP:  words[4],
					ReqID:     words[6],
					Operation: words[7],
					Key:       words[8],
					HTTPCode:  words[12],
					ErrorCode: words[13],
					BytesSent: bs,
					TotalTime: dur}

				// APPEND (safe due to zero value for slices)
				key := log.ReqRecv.Format(keyLongForm)
				index[key] = append(index[key], log)
			}
		}
		if err := scanner.Err(); err != nil {
			log.Fatal(err)
		}
		file.Close()
	}
	fmt.Fprintf(os.Stderr, "Files indexed: %d\n", len(files))
	return index
}

func getHomeDir() string {
	if runtime.GOOS == "windows" {
		home := os.Getenv("HOMEDRIVE") + os.Getenv("HOMEPATH")
		if home == "" {
			home = os.Getenv("USERPROFILE")
		}
		return home
	}
	return os.Getenv("HOME")
}
func timeT(w io.Writer, start time.Time, name string) {
	elapsed := time.Since(start)
	fmt.Fprintf(w, "%s required: %s\n", name, elapsed)
	//fmt.Printf("Indexing took: %0.2f sec\n", elapsed)
}
